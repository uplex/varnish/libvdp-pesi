..
	Copyright 2023 UPLEX Nils Goroll Systemoptimierung
	SPDX-License-Identifier: BSD-2-Clause
	See LICENSE file for full text of license

===================
About this document
===================

.. keep this section at the top!

This document contains a log of noteworthy changes to libvdp_pesi from new
to old.

===================
libvdp_pesi Changes
===================

NEXT
----

.. #13 was a regression introduced since 7.2, so it does not qualify
   as a change herein

* Implement ``onerror`` attribute like varnish-cache (`#6`_), except
  for one difference: Because pESI works in parallel, it runs into
  errors earlier and thus may output less data than serial ESI in
  Varnish-Cache.

* Fixed a panic when ``max_esi_depth`` was reached (`#11`_).

* Achieved an "all green" coverage report (>95% by lines, 100% by
  functions)

* Added use of the new ``$Restrict`` feature from varnish-cache to
  ensure that key pESI functions are only called from ``vcl_deliver
  {}``.

* Optimized handling of uncacheable objects: An improvement in
  varnish-cache made it possible to avoid copying.

* Fixed a bug which could surface as a panic when worker threads
  were destroyed (`#14`_)

* Improved documentation

* Reduced memory required per pESI request

* Various refactoring

* Adjusted to vxid and other changes in varnish-cache

.. _`#6`: https://gitlab.com/uplex/varnish/libvdp-pesi/-/issues/6
.. _`#11`: https://gitlab.com/uplex/varnish/libvdp-pesi/-/issues/11
.. _`#14`: https://gitlab.com/uplex/varnish/libvdp-pesi/-/issues/14

1097f6f48a8ea89fed89227965d94f630fb93c1f / 7.2 branch
-----------------------------------------------------

Base of the changelog

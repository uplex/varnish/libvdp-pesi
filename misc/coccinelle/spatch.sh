#!/bin/bash

function usage() {
    cat <<EOF
Usage: $0 <sp-file> <spatch-arguments>

Examples:

* apply

  $0 my.cocci --dir . --in-place

* expand a patch

  $0 my.cocci --parse-cocci
EOF
}

if [[ ${#@} -lt 1 ]] ; then
    usage
    exit
fi

if [[ -z "${VARNISHSRC}" ]] ; then
    echo VARNISHSRC must be set, pointing to the Varnish-Cache source tree
    exit
fi

cocci="${1}"
shift

spatch=(
    spatch
    --macro-file ${VARNISHSRC}/tools/coccinelle/vdef.h
    -I ${VARNISHSRC}/include/
    -I ${VARNISHSRC}/bin/varnishd/ "${@}"
    --sp-file "${cocci}"
)

exec "${spatch[@]}"

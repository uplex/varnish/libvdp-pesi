==============================
Parallel ESI for Varnish-Cache
==============================

**THIS BRANCH IS FOR VARNISH-CACHE MASTER >= 7.5 and trunk ONLY**

See branches for support of older versions.

.. role:: ref(emphasis)

.. _Varnish-Cache: https://varnish-cache.org/

This project provides parallel ESI processing for `Varnish-Cache`_ as
a module (VMOD).

PROJECT RESOURCES
=================

* The primary repository is at https://code.uplex.de/uplex-varnish/libvdp-pesi

  This server does not accept user registrations, so please use ...

* the mirror at https://gitlab.com/uplex/varnish/libvdp-pesi for issues,
  merge requests and all other interactions.

INTRODUCTION
============

.. _Standard ESI processing: https://varnish-cache.org/docs/trunk/users-guide/esi.html

`Standard ESI processing`_ in `Varnish-Cache`_ is sequential. In
short, it works like this:

1. Process the (sub)request

2. For a cache-miss or pass, fetch the requested object and parse it
   on the backend side, if ESI parsing is enabled. Store the object in
   a parsed, pre-segmented form.

3. Back on the client side, process the parsed, pre-segmented ESI
   object. For all includes, create a sub-request and start with it at
   step 1.

Simply put, this process is very efficient if Step 2 does not need to
be done because the requested object is already in cache.

Conversely, the total time it takes to generate an ESI response is
roughly the sum of all fetch times.

This is where parallel ESI processing can help: In step 3, all the
sub-requests for any particular object are run in parallel, such that
the total time it takes to generate an ESI response at a particular
include level is reduced to the longest of the fetch times.

"At a particular include level" is important, because the optimization
only helps if there are many includes at a particular level: For
example, if object A includes object B, which includes object C and no
object is cacheable, they still need to be fetched in order: The
request for B can only be started once A is available, and likewise
for B and C.

To summarize:

* Parallel ESI can *substantially* improve the response times for ESI
  if cacheable objects include many uncacheable objects. The maximum
  benefit, compared with standard, serial processing, is achieved
  through parallel ESI in cases where all nodes of an ESI tree are
  cacheable and at least some leaves are not.

* If basically all objects are cacheable, parallel ESI only provides a
  relevant benefit on an empty cache, or if cache TTLs are low, such
  that cache misses are likely.

Example
-------

Consider this ESI tree, where an object A includes B1 and B2, which,
in turn, include C1 to C3 and C4 to C6, respectively::

          A
       __/ \__
      /       \
     B1       B2
   / |  \   / |  \
  C1 C2 C3 C4 C5 C6

Let's assume that A, B1 and B2 are cacheable and already in cache and
all C objects are uncacheable (passes). Let's also assume that C1 to
C6 take their number times 100ms to fetch from the backend - that is,
C1 takes 100ms, C2 200ms etc.

With `Standard ESI processing`_, the total response time will be
roughly 100ms + 200ms + ... 600ms = 2100ms = 2.1s. If the response is
a web page, the top bit will load relatively fast, the next part half
as fast, the third part again 100ms slower etc.

With parallel ESI, the total response time will be roughly 600ms =
0.6s. There will still be a delay for each fragment of the page, but
it will be 100ms for each part.

REQUIREMENTS
============

All versions of the VDP require strict ABI compatibility with Varnish,
meaning that it must run against the same build version of Varnish as
the version against which the VDP was built. This means that the
"commit id" portion of the Varnish version string (the SHA1 hash) must
be the same at runtime as at build time.

INSTALLATION
============

See `INSTALL.rst <INSTALL.rst>`_ in the source repository.

TL;DR: QUICK START
==================

The full documentation of the VMOD is in :ref:`vmod_pesi(3)`. If you
are reading this document online, it should be available as
`vmod_pesi.man.rst <src/vmod_pesi.man.rst>`_.

The full documentation is detailed on purpose. It aims to explain well
how this VMOD works and how optimizations can be tuned.

We welcome all users to read the documentation, but many users will
neither want to nor need to understand the details. Thus, here is what
you *really* need to know:

* See `INSTALL.rst <INSTALL.rst>`_ in the source repository for
  installation instructions.

* To use pESI, add to the top of your VCL::

    import pesi;

  and to your ``sub vcl_deliver {}``, add::

    pesi.activate();

  This should be added *after* any modification of ``resp.do_esi``,
  ``req.http.Accept-Encoding``, ``req.http.Range`` or
  ``resp.filters``, if these exist.

  To be safe, ``pesi.activate()`` can be called before any
  ``return(deliver)`` in ``sub vcl_deliver {}``.

* If you call ``pesi.activate()``, call it unconditionally and on all
  ESI levels. Read the detailed documentation for more information.

It is possible that your current configuration of system resources,
such as thread pools, workspaces, memory allocation and so forth, will
suffice after this simple change, and will need no further
optimization.

But that is by no means ensured, since pESI uses system resources
differently from standard ESI. Understanding these difference, and how
to monitor and manage resource usage affected by pESI, is a main focus
of the detailed discussion in `vmod_pesi(3) <src/vmod_pesi.man.rst>`_.

ACKNOWLEDGEMENTS
================

.. _Otto GmbH & Co KG: https://www.otto.de/

Most of the development work of this vmod in 2019 and 2020 has been
sponsored by `Otto GmbH & Co KG`_.

.. _BoardGameGeek: https://boardgamegeek.com/

The initial release to the public in 2021 has been supported by
`BoardGameGeek`_.

SUPPORT
=======

.. _gitlab.com issues: https://gitlab.com/uplex/varnish/libvdp-pesi/-/issues

To report bugs, use `gitlab.com issues`_.

For enquiries about professional service and support, please contact
info@uplex.de\ .

CONTRIBUTING
============

.. _merge requests on gitlab.com: https://gitlab.com/uplex/varnish/libvdp-pesi/-/merge_requests

To contribute to the project, please use `merge requests on gitlab.com`_.

To support the project's development and maintenance, there are
several options:

.. _paypal: https://www.paypal.com/donate/?hosted_button_id=BTA6YE2H5VSXA

.. _github sponsor: https://github.com/sponsors/nigoroll

* Donate money through `paypal`_. If you wish to receive a commercial
  invoice, please add your details (address, email, any requirements
  on the invoice text) to the message sent with your donation.

* Become a `github sponsor`_.

* Contact info@uplex.de to receive a commercial invoice for SWIFT payment.

COPYRIGHT
=========

::

  Copyright 2019 - 2021 UPLEX Nils Goroll Systemoptimierung
  All rights reserved
 
  Authors: Geoffrey Simmons <geoffrey.simmons@uplex.de>
           Nils Goroll <nils.goroll@uplex.de>
 
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

#ifdef DEBUG
#include <stdio.h>
#include "vtim.h"
#define Debug(fmt, ...) printf(fmt, __VA_ARGS__)
#define VSLdbgv(tng, fmt, ...)						\
	do {								\
		VSL(SLT_Debug, NO_VXID, "xid=%lu t=%.6f " fmt,			\
		    VXID((tng)->vsl->wid), VTIM_real(), __VA_ARGS__);	\
		VSLb((tng)->vsl, SLT_Debug, fmt, __VA_ARGS__);		\
	} while(0)

#define VSLdbg(tng, msg)				\
	do {						\
		VSL(SLT_Debug, NO_VXID, "xid=%lu t=%.6f " msg,	\
		    VXID((tng)->vsl->wid), VTIM_real());	\
		VSLb((tng)->vsl, SLT_Debug, msg);		\
	} while(0)
#define VSL0dbg(...) VSL(SLT_Debug, NO_VXID, __VA_ARGS__)
#else
#define VSLdbgv(tng, fmt, ...)	(void)0
#define VSLdbg(tng, msg)	(void)0
#define VSL0dbg(...)	(void)0
#define Debug(fmt, ...) /**/
#endif

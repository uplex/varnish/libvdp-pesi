varnishtest "gzipped level 1 in level 0 plain"

server s1 {
	rxreq
	txresp -body {<Before esi1>
<esi:include src="/esi1.html"/>
Between esi1 and esi2
<esi:include src="/esi2.html"/>
After esi2
}
} -start

server s2 {
	rxreq
	expect req.url == "/esi1.html"
	txresp -gzipbody {<Before esi1-1>
<esi:include src="/esi1-1.html"/>
Between esi1-1 and esi1-2
<esi:include src="/esi1-2.html"/>
After esi1-2
}
} -start

server s3 {
	rxreq
	expect req.url == "/esi2.html"
	txresp -gzipbody {<Before esi2-1>
<esi:include src="/esi2-1.html"/>
Between esi2-1 and esi2-2
<esi:include src="/esi2-2.html"/>
After esi2-2
}
} -start

server s4 {
	loop 4 {
		rxreq
		expect req.url == "/esi1-1.html"
		txresp -hdr "Cache-Control: max-age=0" -gzipbody {esi1-1
}
	}
} -start

server s5 {
	loop 4 {
		rxreq
		expect req.url == "/esi1-2.html"
		txresp -hdr "Cache-Control: max-age=0" -gzipbody {esi1-2
}
	}
} -start

server s6 {
	loop 4 {
		rxreq
		expect req.url == "/esi2-1.html"
		txresp -hdr "Cache-Control: max-age=0" -gzipbody {esi2-1
}
	}
} -start

server s7 {
	loop 4 {
		rxreq
		expect req.url == "/esi2-2.html"
		txresp -hdr "Cache-Control: max-age=0" -gzipbody {esi2-2
}
	}
} -start

varnish v1 -vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";

	sub vcl_backend_fetch {
		if (bereq.url == "/") {
			set bereq.backend = s1;
		}
		elsif (bereq.url == "/esi1.html") {
			set bereq.backend = s2;
		}
		elsif (bereq.url == "/esi2.html") {
			set bereq.backend = s3;
		}
		elsif (bereq.url == "/esi1-1.html") {
			set bereq.backend = s4;
		}
		elsif (bereq.url == "/esi1-2.html") {
			set bereq.backend = s5;
		}
		elsif (bereq.url == "/esi2-1.html") {
			set bereq.backend = s6;
		}
		elsif (bereq.url == "/esi2-2.html") {
			set bereq.backend = s7;
		}
	}

	sub vcl_backend_response {
		set beresp.do_esi = true;
	}

	sub vcl_deliver {
		pesi.activate();
	}
} -start

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.bodylen == 191
	expect resp.body == {<Before esi1>
<Before esi1-1>
esi1-1

Between esi1-1 and esi1-2
esi1-2

After esi1-2

Between esi1 and esi2
<Before esi2-1>
esi2-1

Between esi2-1 and esi2-2
esi2-2

After esi2-2

After esi2
}
} -run

## HTTP/2

varnish v1 -cliok "param.set feature +http2"

## See comments in e23.vtc about limitations using vtc to test
## gzipped H2 responses.

client c1 {
	stream 1 {
		txreq
		rxresp
		expect resp.status == 200
		expect resp.bodylen == 191
		expect resp.body == {<Before esi1>
<Before esi1-1>
esi1-1

Between esi1-1 and esi1-2
esi1-2

After esi1-2

Between esi1 and esi2
<Before esi2-1>
esi2-1

Between esi2-1 and esi2-2
esi2-2

After esi2-2

After esi2
}
	} -run
} -run

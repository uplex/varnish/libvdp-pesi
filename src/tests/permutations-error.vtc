varnishtest "permutations of esion/esioff gzip/plain fetch/pass"

#
# we always get the same ESI object, but modify it in VCL under
# different URLs
# /[eE][GP][fp]
#   ^   ^   ^--- (f)etch / (p)ass
#   |   +------- (G)zip / (P)plain
#   +----------- (e)si off / (E)SI on
#
# backend: /
#
# /O[GP][fp]
#
# backend: /O

server s1 -repeat 2 -keepalive {
	rxreq
	expect req.url == "/"
	txresp -body {<esi:include src="/EGf"/><esi:include src="/EGp"/>
<esi:include src="/OGf"/><esi:include src="/OGp"/>
<esi:include src="/EPf"/><esi:include src="/EPp"/>
<esi:include src="/OPf"/><esi:include src="/OPp"/>
<esi:include src="/eGf"/><esi:include src="/eGp"/>
<esi:include src="/ePf"/><esi:include src="/ePp"/>
}
} -start

server s2 -repeat 2 -keepalive {
	rxreq
	expect req.url == "/O"
	txresp -nolen -hdr "Transfer-encoding: chunked"
	chunked {</BEGIN O>}
	chunkedlen 256
	chunkedlen 512
	chunked {</END O>}
	chunkedlen 0
} -start

varnish v1 -arg "-a 127.0.0.1:0 -a self=${tmpdir}/v1.sock,mode=777" \
	-arg "-p first_byte_timeout=5" \
	-arg "-p between_bytes_timeout=5" \
	-arg "-p backend_idle_timeout=1" \
	-arg "-p thread_pool_min=128" \
	-arg "-l 80m" \
	-vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";
	import vtc;
	import std;

        backend self {
            .path = "${tmpdir}/v1.sock";
        }

        sub vcl_synth {
            vtc.sleep(std.duration(real=std.random(0,1)));
            synthetic("*" + resp.status + "*");
            return (deliver);
        }

        sub vcl_hash {
            if (req.url ~ "^/[eE][GP][fp]$") {
                set req.url = regsub(req.url,
                  "^/[eE]([GP])[fp]$", "/\1");
            }
            elsif (req.url ~ "^/O[GP][fp]$") {
                set req.url = regsub(req.url,
                  "^/(O[GP])[fp]$", "/\1");
            }
            elsif (local.socket == "self" &&
              req.url ~ "^/O?[GP]$") {
                # OK
            }
            else {
                return (fail);
            }
            hash_data(req.url);
            return (lookup);
        }

        sub vcl_recv {
            set req.http.url = req.url;

            if (req.esi_level == 3) {
                return (synth(200));
            }

            if (local.socket == "self") {
                return (hash);
            }

            if (req.url ~ "p$") {
                return (pass);
            }
            return (hash);
        }

        sub vcl_deliver {
            if (local.socket == "self") {
                set resp.do_esi = false;
                return (deliver);
            }
            if (req.http.url ~ "^/E") {
                set resp.do_esi = true;
            }
            else {
                set resp.do_esi = false;
            }
            pesi.activate();
        }

        sub vcl_backend_fetch {
            if (bereq.uncacheable) {
                set bereq.backend = self;
                return (fetch);
            }
            # url is /[GP] or /O[GP] - we want one cache object for
            # plain and gzip each
            if (bereq.url ~ "G$") {
                set bereq.http.Accept-Encoding = "gzip";
            }
            else {
                unset bereq.http.Accept-Encoding;
            }

            set bereq.url = regsub(bereq.url, ".$", "");
            if (bereq.url == "/") {
                set bereq.backend = s1;
            }
            elsif (bereq.url == "/O") {
                set bereq.backend = s2;
            }
            else {
                return (abandon);
            }
            return (fetch);
        }

        sub vcl_backend_response {
            if (bereq.url !~ "^/O") {
                set beresp.do_esi = true;
            }
            if (bereq.uncacheable) {
                return (deliver);
            }
            if (bereq.http.Accept-Encoding) {
                set beresp.do_gzip = true;
            }
            return (deliver);
        }
} -start


# this masks out most of the VSL to push the output size just under
# the 1MB default varnishtest log buffer size
#
# To get the full output, comment these out and run varnishtest with -b 10m
varnish v1 -cliok "param.set vsl_mask -RespHeader,-BerespHeader"
varnish v1 -cliok "param.set vsl_mask -ReqHeader,-BereqHeader,-Timestamp"
varnish v1 -cliok "param.set vsl_mask -ReqUnset,-BereqUnset"
varnish v1 -cliok "param.set vsl_mask -RespUnset,-BerespUnset"
varnish v1 -cliok "param.set vsl_mask -ReqMethod,-BereqMethod"
varnish v1 -cliok "param.set vsl_mask -ReqProtocol,-BereqProtocol"
varnish v1 -cliok "param.set vsl_mask -ReqProtocol,-BereqProtocol"
varnish v1 -cliok "param.set vsl_mask -Storage,-VCL_call,-VCL_return"
varnish v1 -cliok "param.set vsl_mask -RespStatus,-RespReason,-RespProtocol"
varnish v1 -cliok "param.set vsl_mask -BerespStatus,-BerespReason,-BerespProtocol"
varnish v1 -cliok "param.set vsl_mask -ReqAcct,-ReqStart"
varnish v1 -cliok "param.set vsl_mask -Begin,-End,-SessOpen,-Link"
varnish v1 -cliok "param.set vsl_mask -BackendOpen"

client c1 -connect ${tmpdir}/v1.sock {
	txreq -url "/G" -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.Content-Encoding == "gzip"
	expect resp.status == 200
	txreq -url "/P" -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.Content-Encoding == <undef>
	expect resp.status == 200

	txreq -url "/OG" -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.Content-Encoding == "gzip"
	expect resp.status == 200
	txreq -url "/OP" -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.Content-Encoding == <undef>
	expect resp.status == 200
} -run

client c2 {
	txreq -url "/OG"
	rxresp
	expect resp.status == 503
} -run

# gzip obj

client c2 {
	txreq -url "/EGf"
	rxresphdrs
	expect resp.status == 200
	expect resp.http.Content-Encoding == <undef>
	rxchunk
} -start

client c3 {
	txreq -url "/EGp"
	rxresphdrs
	expect resp.status == 200
	expect resp.http.Content-Encoding == <undef>
	rxchunk
} -start

# plain obj

client c4 {
	txreq -url "/EPf"
	rxresphdrs
	expect resp.status == 200
	expect resp.http.Content-Encoding == <undef>
	rxchunk
} -start

client c5 {
	txreq -url "/EPp"
	rxresphdrs
	expect resp.status == 200
	expect resp.http.Content-Encoding == <undef>
	rxchunk
} -start

client c2 -wait
client c3 -wait
client c4 -wait
client c5 -wait

varnish v1 -vsc *

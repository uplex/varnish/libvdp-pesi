varnishtest "ESI include"


server s1 {
	rxreq
	expect req.http.esi0 == "foo"
	txresp -noserver -body {
		<html>
		Before include
		<esi:include src="/body" sr="foo"/>
		After include
		</html>
	}
} -start

server s2 {
	rxreq
	expect req.url == "/body1"
	expect req.http.esi0 != "foo"
	txresp -noserver -body {
		Included file
	}
} -start

varnish v1 -arg "-p debug=+syncvsl" -vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";

	sub vcl_recv {
		if (req.esi_level > 0) {
			set req.url = req.url + req.esi_level;
		} else {
			set req.http.esi0 = "foo";
		}
	}

	sub vcl_backend_fetch {
		if (bereq.url == "/") {
			set bereq.backend = s1;
		}
		else {
			set bereq.backend = s2;
		}
	}

	sub vcl_backend_response {
		if (bereq.url == "/") {
			set beresp.do_esi = true;
		}
	}

	sub vcl_deliver {
		pesi.activate();
		set resp.http.can_esi = obj.can_esi;
		unset resp.http.via;
	}
} -start

client c1 {
	txreq -nouseragent -hdr "Host: foo"
	rxresp
	expect resp.status == 200
	expect resp.bodylen == 75
	expect resp.http.can_esi == "true"
	expect resp.body == {
		<html>
		Before include
		
		Included file
	
		After include
		</html>
	}

	delay .1
	# test that there is no difference on miss/hit
	txreq -nouseragent -hdr "Host: foo"
	rxresp
	expect resp.status == 200
	expect resp.bodylen == 75
	expect resp.http.can_esi == "true"
	expect resp.body == {
		<html>
		Before include
		
		Included file
	
		After include
		</html>
	}
}

# Because ReqAcct includes chunk headers, depending on the order of
# events and whether or not we use (partial) sequential delivery (for
# example, when no threads are available), ReqAcct adds n x 8 to the
# net data size.
#
# in this test case we see either one or two chunk headers in addition
# to the end chunk.

client c1 -run
varnish v1 -expect esi_errors == 0
varnish v1 -expect MAIN.s_resp_bodybytes == 150
delay 1

logexpect l1 -v v1 -d 1 -g vxid -q "vxid == 1001" {
	expect 0 1001   Begin   "^req .* rxreq"
	expect * =	ReqAcct	"^29 0 29 170 75 245$"
	expect 0 =      End
} -run

logexpect l2 -v v1 -d 1 -g vxid -q "vxid == 1002" {
	expect * 1002   Begin   "^bereq "
	expect * =      End
} -run

logexpect l3 -v v1 -d 1 -g vxid -q "vxid == 1003" {
	expect * 1003   Begin   "^req .* esi"
	expect * =	ReqAcct	"^0 0 0 0 18 18$"
	expect 0 =      End
} -run

logexpect l4 -v v1 -d 1 -g vxid -q "vxid == 1004" {
	expect * 1004   Begin   "^bereq "
	expect * =      End
} -run


logexpect l5 -v v1 -d 1 -g vxid -q "vxid == 1005" {
	expect * 1005   Begin   "^req .* rxreq"
	# Header bytes is 5 larger than in l1 due to two item X-Varnish hdr
	expect * =	ReqAcct	"^29 0 29 175 75 250$"
	expect 0 =      End
} -run

logexpect l6 -v v1 -d 1 -g vxid -q "vxid == 1006" {
	expect * 1006   Begin   "^req .* esi"
	expect * =	ReqAcct	"^0 0 0 0 18 18$"
	expect 0 =      End
} -run

## HTTP/2

varnish v1 -cliok "param.set feature +http2"

client c1 {
	stream 1 {
		txreq -hdr host foo
		rxresp
		expect resp.status == 200
		expect resp.bodylen == 75
		expect resp.body == {
		<html>
		Before include
		
		Included file
	
		After include
		</html>
	}
	} -run
} -run

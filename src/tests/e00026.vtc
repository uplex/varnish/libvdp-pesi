varnishtest "Test esi-include + gzip edgecase with respect to gzip hdr"

server s1 {
	rxreq
	expect req.url == "/foo"
	txresp -gzipbody {<h1>/////<h2>}

	rxreq
	expect req.url == "/1"
	expect req.http.accept-encoding == gzip
	txresp -gzipbody {B<esi:include src="/foo"/>A}

	rxreq
	expect req.url == "/2"
	expect req.http.accept-encoding == gzip
	txresp -gzipbody {B<esi:include src="/foo"/>}

	rxreq
	expect req.url == "/3"
	expect req.http.accept-encoding == gzip
	txresp -gzipbody {<esi:include src="/foo"/>A}

	rxreq
	expect req.url == "/4"
	expect req.http.accept-encoding == gzip
	txresp -gzipbody {<esi:include src="/foo"/>}

} -start

varnish v1 -vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";

	sub vcl_backend_response {
		if (bereq.url != "/foo") {
			set beresp.do_esi = true;
		}
	}

	sub vcl_deliver {
		pesi.activate();
	}
} -start

varnish v1 -cliok "param.set feature +esi_disable_xml_check"

varnish v1 -cliok "param.set debug +syncvsl,+workspace"

client c1 {
	txreq -url /foo -hdr "Accept-Encoding: gzip"
	rxresp
	gunzip
	expect resp.status == 200
	expect resp.bodylen == 13
	expect resp.body == {<h1>/////<h2>}

	txreq -url /1 -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.content-encoding == gzip
	gunzip
	expect resp.status == 200
	expect resp.bodylen == 15
	expect resp.body == {B<h1>/////<h2>A}

	txreq -url /2 -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.content-encoding == gzip
	gunzip
	expect resp.status == 200
	expect resp.bodylen == 14
	expect resp.body == {B<h1>/////<h2>}

	txreq -url /3 -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.content-encoding == gzip
	gunzip
	expect resp.status == 200
	expect resp.bodylen == 14
	expect resp.body == {<h1>/////<h2>A}

	txreq -url /4 -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.content-encoding == gzip
	gunzip
	expect resp.status == 200
	expect resp.bodylen == 13
	expect resp.body == {<h1>/////<h2>}

}

client c1 -run
varnish v1 -expect esi_errors == 0

## HTTP/2

varnish v1 -cliok "param.set feature +http2"

## See comments in e23.vtc about limitations using vtc to test
## gzipped H2 responses.

client c1 {
	stream 1 {
		txreq -url /foo -hdr accept-encoding gzip
		rxresp
		expect resp.http.content-encoding == gzip
		expect resp.status == 200
	} -run
	stream 3 {
		txreq -url /1 -hdr accept-encoding gzip
		rxresp
		expect resp.http.content-encoding == gzip
		expect resp.status == 200
	} -run
	stream 5 {
		txreq -url /2 -hdr accept-encoding gzip
		rxresp
		expect resp.http.content-encoding == gzip
		expect resp.status == 200
	} -run
	stream 7 {
		txreq -url /3 -hdr accept-encoding gzip
		rxresp
		expect resp.http.content-encoding == gzip
		expect resp.status == 200
	} -run
	stream 9 {
		txreq -url /4 -hdr accept-encoding gzip
		rxresp
		expect resp.http.content-encoding == gzip
		expect resp.status == 200
	} -run
} -run

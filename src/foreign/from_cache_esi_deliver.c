/*-
 * Copyright (c) 2011 Varnish Software AS
 * All rights reserved.
 *
 * Author: Poul-Henning Kamp <phk@phk.freebsd.dk>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * VPED - Varnish Parallel Esi Delivery
 *
 * modded version for parallel PESI
 *
 * actual esi handling is in vdp_pesi.c, this file contains VDPs we keep
 * functionally equivalent to varnish-cache built-in esi, just with changed
 * signatures & data structures
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "cache/cache.h"
#include "cache/cache_filter.h"

//#include "vtim.h"
//#include "cache_esi.h"
#include "vend.h"
#include "vgz.h"

#include "from_cache_esi_deliver.h"

/*---------------------------------------------------------------------
 * If a gzip'ed ESI object includes a ungzip'ed object, we need to make
 * it looked like a gzip'ed data stream.  The official way to do so would
 * be to fire up libvgz and gzip it, but we don't, we fake it.
 *
 * First, we cannot know if it is ungzip'ed on purpose, the admin may
 * know something we don't.
 *
 * What do you mean "BS ?"
 *
 * All right then...
 *
 * The matter of the fact is that we simply will not fire up a gzip in
 * the output path because it costs too much memory and CPU, so we simply
 * wrap the data in very convenient "gzip copy-blocks" and send it down
 * the stream with a bit more overhead.
 */

static int v_matchproto_(vdp_fini_f)
vped_pretend_gzip_fini(struct vdp_ctx *vdc, void **priv)
{
	(void)vdc;
	*priv = NULL;
	return (0);
}

static int v_matchproto_(vdp_bytes_f)
vped_pretend_gzip_bytes(struct vdp_ctx *vdx, enum vdp_action act, void **priv,
    const void *pv, ssize_t l)
{
	uint8_t buf1[5], buf2[5];
	const uint8_t *p;
	uint16_t lx;
	struct nexus_gzip *gz;

	CHECK_OBJ_NOTNULL(vdx, VDP_CTX_MAGIC);
	CAST_OBJ_NOTNULL(gz, *priv, NEXUS_GZIP_MAGIC);

	if (l == 0)
		return (VDP_bytes(vdx, act, pv, l));

	p = pv;

	AN (gz->is);
	gz->crc = crc32(gz->crc, p, l);
	gz->l_crc += l;

	lx = 65535;
	buf1[0] = 0;
	vle16enc(buf1 + 1, lx);
	vle16enc(buf1 + 3, ~lx);

	while (l > 0) {
		if (l >= 65535) {
			lx = 65535;
			if (VDP_bytes(vdx, VDP_NULL, buf1, sizeof buf1))
				return (-1);
		} else {
			lx = (uint16_t)l;
			buf2[0] = 0;
			vle16enc(buf2 + 1, lx);
			vle16enc(buf2 + 3, ~lx);
			if (VDP_bytes(vdx, VDP_NULL, buf2, sizeof buf2))
				return (-1);
		}
		if (VDP_bytes(vdx, VDP_NULL, p, lx))
			return (-1);
		l -= lx;
		p += lx;
	}
	/* buf1 & buf2 is local, have to flush */
	return (VDP_bytes(vdx, VDP_FLUSH, NULL, 0));
}

const struct vdp vped_pretend_gz = {
	.name =		"PPGZ",
	.bytes =	vped_pretend_gzip_bytes,
	.fini =		vped_pretend_gzip_fini,
};

/*---------------------------------------------------------------------
 * Include a gzip'ed object in a gzip'ed ESI object delivery
 *
 * This is the interesting case: Deliver all the deflate blocks, stripping
 * the "LAST" bit of the last one and padding it, as necessary, to a byte
 * boundary.
 *
 */

static int v_matchproto_(vdp_fini_f)
vped_gzgz_init(VRT_CTX, struct vdp_ctx *vdc, void **priv)
{
	ssize_t l;
	const char *p;
	struct vped_gzgz_priv *foo;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vdc, VDP_CTX_MAGIC);
	AN(priv);
	CAST_OBJ_NOTNULL(foo, *priv, VPED_GZGZ_PRIV_MAGIC);
	CHECK_OBJ_NOTNULL(foo->objcore, OBJCORE_MAGIC);

	memset(foo->tailbuf, 0xdd, sizeof foo->tailbuf);

	AN(ObjCheckFlag(vdc->wrk, foo->objcore, OF_GZIPED));

	p = ObjGetAttr(vdc->wrk, foo->objcore, OA_GZIPBITS, &l);
	AN(p);
	assert(l == 32);
	foo->start = vbe64dec(p);
	foo->last = vbe64dec(p + 8);
	foo->stop = vbe64dec(p + 16);
	foo->olen = ObjGetLen(vdc->wrk, foo->objcore);
	assert(foo->start > 0 && foo->start < foo->olen * 8);
	assert(foo->last > 0 && foo->last < foo->olen * 8);
	assert(foo->stop > 0 && foo->stop < foo->olen * 8);
	assert(foo->last >= foo->start);
	assert(foo->last < foo->stop);

	/* The start bit must be byte aligned. */
	AZ(foo->start & 7);
	return (0);
}

static int v_matchproto_(vdp_bytes_f)
vped_gzgz_bytes(struct vdp_ctx *vdx, enum vdp_action act, void **priv,
    const void *ptr, ssize_t len)
{
	struct vped_gzgz_priv *foo;
	const uint8_t *pp;
	ssize_t dl;
	ssize_t l;

	// XXX would like to know which is the last
	// segment sent to push FLUSH/END with it

	CAST_OBJ_NOTNULL(foo, *priv, VPED_GZGZ_PRIV_MAGIC);
	pp = ptr;
	if (len > 0) {
		/* Skip over the GZIP header */
		dl = foo->start / 8 - foo->ll;
		if (dl > 0) {
			/* Before foo.start, skip */
			if (dl > len)
				dl = len;
			foo->ll += dl;
			len -= dl;
			pp += dl;
		}
	}
	if (len > 0) {
		/* The main body of the object */
		dl = foo->last / 8 - foo->ll;
		if (dl > 0) {
			if (dl > len)
				dl = len;
			if (VDP_bytes(vdx, VDP_NULL, pp, dl))
				return(-1);
			foo->ll += dl;
			len -= dl;
			pp += dl;
		}
	}
	if (len > 0 && (unsigned)foo->ll == foo->last / 8) {
		/* Remove the "LAST" bit */
		foo->dbits[0] = *pp;
		foo->dbits[0] &= ~(1U << (foo->last & 7));
		if (VDP_bytes(vdx, VDP_NULL, foo->dbits, 1))
			return (-1);
		foo->ll++;
		len--;
		pp++;
	}
	if (len > 0) {
		/* Last block */
		dl = foo->stop / 8 - foo->ll;
		if (dl > 0) {
			if (dl > len)
				dl = len;
			if (VDP_bytes(vdx, VDP_NULL, pp, dl))
				return (-1);
			foo->ll += dl;
			len -= dl;
			pp += dl;
		}
	}
	if (len > 0 && (foo->stop & 7) && (unsigned)foo->ll == foo->stop / 8) {
		/* Add alignment to byte boundary */
		foo->dbits[1] = *pp;
		foo->ll++;
		len--;
		pp++;
		switch ((int)(foo->stop & 7)) {
		case 1: /*
			 * x000....
			 * 00000000 00000000 11111111 11111111
			 */
		case 3: /*
			 * xxx000..
			 * 00000000 00000000 11111111 11111111
			 */
		case 5: /*
			 * xxxxx000
			 * 00000000 00000000 11111111 11111111
			 */
			foo->dbits[2] = 0x00; foo->dbits[3] = 0x00;
			foo->dbits[4] = 0xff; foo->dbits[5] = 0xff;
			foo->lpad = 5;
			break;
		case 2: /* xx010000 00000100 00000001 00000000 */
			foo->dbits[1] |= 0x08;
			foo->dbits[2] = 0x20;
			foo->dbits[3] = 0x80;
			foo->dbits[4] = 0x00;
			foo->lpad = 4;
			break;
		case 4: /* xxxx0100 00000001 00000000 */
			foo->dbits[1] |= 0x20;
			foo->dbits[2] = 0x80;
			foo->dbits[3] = 0x00;
			foo->lpad = 3;
			break;
		case 6: /* xxxxxx01 00000000 */
			foo->dbits[1] |= 0x80;
			foo->dbits[2] = 0x00;
			foo->lpad = 2;
			break;
		case 7:	/*
			 * xxxxxxx0
			 * 00......
			 * 00000000 00000000 11111111 11111111
			 */
			foo->dbits[2] = 0x00;
			foo->dbits[3] = 0x00; foo->dbits[4] = 0x00;
			foo->dbits[5] = 0xff; foo->dbits[6] = 0xff;
			foo->lpad = 6;
			break;
		case 0: /* xxxxxxxx */
		default:
			WRONG("compiler must be broken");
		}
		if (VDP_bytes(vdx, VDP_NULL, foo->dbits + 1, foo->lpad))
			return (-1);
	}
	if (len > 0) {
		/* Recover GZIP tail */
		dl = foo->olen - foo->ll;
		assert(dl >= 0);
		if (dl > len)
			dl = len;
		if (dl > 0) {
			assert(dl <= 8);
			l = foo->ll - (foo->olen - 8);
			assert(l >= 0);
			assert(l <= 8);
			assert(l + dl <= 8);
			memcpy(foo->tailbuf + l, pp, dl);
			foo->ll += dl;
			len -= dl;
		}
	}
	assert(len == 0);
	if (act != VDP_END)
		act = VDP_FLUSH;
	return (VDP_bytes(vdx, act, NULL, 0));
}

static int v_matchproto_(vdp_fini_f)
vped_gzgz_fini(struct vdp_ctx *vdc, void **priv)
{
	uint32_t icrc;
	uint32_t ilen;
	struct vped_gzgz_priv *vgzgz;

	(void) vdc;

	CAST_OBJ_NOTNULL(vgzgz, *priv, VPED_GZGZ_PRIV_MAGIC);
	*priv = NULL;

	icrc = vle32dec(vgzgz->tailbuf);
	ilen = vle32dec(vgzgz->tailbuf + 4);
	vgzgz->gz->crc = crc32_combine(vgzgz->gz->crc, icrc, ilen);
	vgzgz->gz->l_crc += ilen;

	return (0);
}

const struct vdp vped_gzgz = {
	.name =         "PVZZ",
	.init =         vped_gzgz_init,
	.bytes =        vped_gzgz_bytes,
	.fini =         vped_gzgz_fini,
};

/*-
 * Copyright 2021 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <string.h>	// INIT_OBJ
#include <cache/cache.h>
#include <vmb.h>

#include "vcc_pesi_debug_if.h"

#define A(x) assert(x)

/*
 * locking: because we require the PRIV_TOP to be created exactly once and
 * varnish-cache takes care of serializing access to the PRIV_TOP tree itself,
 * we do not need to lock here.
 *
 * see below for how to lock properly for PRIV_TOPs created at lower levels
 */

struct pedbg_top {
	unsigned		magic;
#define PEDBG_TOP_MAGIC	0x94e33a13
	const struct ws	*ws;
	const struct req	*req;
};

struct pedbg_task {
	unsigned		magic;
#define PEDBG_TASK_MAGIC	0x42a71fb0
	const struct ws	*ws;
	const struct req	*req;
};

/*
 * dynamic PRIV_TOP for each esi level
 */
struct pedbg_level {
	unsigned			magic;
#define PEDBG_LEVEL_MAGIC		0x220c28ff
	unsigned			level;
	const struct pedbg_level	*up;
};
/* used as uintptr for pedbg_level */
#define MAX_LEVELS 255
static const char level_id[MAX_LEVELS] = { 'L' };

static void
pedbg_level(VRT_CTX, struct req *topreq, unsigned esi_level)
{
	struct sess *sp;
	struct vmod_priv *p;
	const struct vmod_priv *pup;
	struct pedbg_level *level = NULL;
	const struct pedbg_level *up = NULL;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	assert(esi_level < MAX_LEVELS);

	sp = ctx->sp;
	CHECK_OBJ_NOTNULL(sp, SESS_MAGIC);
	p = VRT_priv_top(ctx, &level_id[esi_level]);

	while (p->priv == NULL) {
		if (esi_level > 0) {
			pup = VRT_priv_top(ctx, &level_id[esi_level - 1]);
			CAST_OBJ_NOTNULL(up, pup->priv, PEDBG_LEVEL_MAGIC);
		}

		Lck_Lock(&sp->mtx);
		if (p->priv != NULL) {
			// raced
			Lck_Unlock(&sp->mtx);
			break;
		}
		level = WS_Alloc(topreq->ws, sizeof *level);
		AN(level);
		INIT_OBJ(level, PEDBG_LEVEL_MAGIC);
		level->level = esi_level;
		level->up = up;
		p->priv = level;
		Lck_Unlock(&sp->mtx);
	}

	if (level == NULL) {
		/* write barrier is implicit in mutex */
		VRMB();
		CAST_OBJ_NOTNULL(level, p->priv, PEDBG_LEVEL_MAGIC);
	}

	A(level->level == esi_level);
	up = level->up;
	while (up) {
		CHECK_OBJ_NOTNULL(up, PEDBG_LEVEL_MAGIC);
		A(up->level == --esi_level);
		up = up->up;
	}
	AZ(esi_level);
}

/* ============================================================
 */

#define PEDBG_SETUP(ctx, ptop, ptask, r, tr)	   \
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);	   \
	AN(ptop);				   \
	AN(ptask);				   \
	r = ctx->req;				   \
	CHECK_OBJ_NOTNULL(r, REQ_MAGIC);	   \
	CHECK_OBJ_NOTNULL(req->top, REQTOP_MAGIC); \
	assert((r)->ws == (ctx)->ws);		   \
	tr = req->top->topreq;			   \
	CHECK_OBJ_NOTNULL(tr, REQ_MAGIC)

/*
 * must be called exactly once
 * restarts check done in vmod for simplicty
 */
VCL_VOID
pedbg_register_privs(VRT_CTX,
    struct vmod_priv *priv_top, struct vmod_priv *priv_task)
{
	struct req *req, *topreq;
	struct pedbg_top *top;
	struct pedbg_task *task;

	PEDBG_SETUP(ctx, priv_top, priv_task, req, topreq);

	if (req->restarts > 0) {
		pedbg_check_privs(ctx, priv_top, priv_task);
		return;
	}

	if (req->esi_level == 0) {
		AZ(priv_top->priv);
		top = WS_Alloc(ctx->ws, sizeof *top);
		AN(top);
		INIT_OBJ(top, PEDBG_TOP_MAGIC);
		top->ws = ctx->ws;
		top->req = req;
		priv_top->priv = top;
	}
	AZ(priv_task->priv);
	task = WS_Alloc(ctx->ws, sizeof *task);
	AN(task);
	INIT_OBJ(task, PEDBG_TASK_MAGIC);
	task->ws = ctx->ws;
	task->req = req;
	priv_task->priv = task;
	pedbg_check_privs(ctx, priv_top, priv_task);
	pedbg_level(ctx, topreq, req->esi_level);
}

/*lint -e{818} vmod_priv are not const as by the interface*/
VCL_VOID
pedbg_check_privs(VRT_CTX,
struct vmod_priv *priv_top,struct vmod_priv *priv_task)
{
	struct req *req, *topreq;
	struct pedbg_top *top;
	struct pedbg_task *task;

	PEDBG_SETUP(ctx, priv_top, priv_task, req, topreq);

	CAST_OBJ_NOTNULL(top, priv_top->priv, PEDBG_TOP_MAGIC);
	CAST_OBJ_NOTNULL(task, priv_task->priv, PEDBG_TASK_MAGIC);

	A(task->ws == ctx->ws);
	A(task->req == ctx->req);

	A(top->req == topreq);
	A(top->ws == topreq->ws);

	if (req->esi_level == 0) {
		A(req == topreq);
		A(top->ws == ctx->ws);
		A(top->req == req);
		return;
	}

	A(top->ws != ctx->ws);
	A(top->req != req);
	A(top->req->ws == top->ws);
	A(top->req->ws != ctx->ws);
}

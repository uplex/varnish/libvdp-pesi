/*-
 * Copyright 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Authors: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *	    Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * node interface
 */

#include "node_head.h"

struct bytes_tree {
	unsigned		magic;
#define BYTES_TREE_MAGIC 0x49c59d46
	int			retval;

	struct lock		tree_lock;
	struct lock		nodes_lock;
	// esi_level > 1 signalling new data
	pthread_cond_t		cond;
	struct node		*root;
	struct node		*front;
	// candidate for last ever node
	struct node		*end;
	int			end_sent;
	int			npending;
};

enum n_type {
	T_INVALID = 0,
	T_NEXUS,	// can change into T_SUBREQ / T_DATA
	T_DATA,
	T_CRC,
	T_SUBREQ
} __attribute__ ((__packed__));

/*
 * see state.dot:
 *
 * ST_DATA:    may never have any children
 *
 *	       T_DATA / T_CRC / T_SUBREQ
 *
 * ST_PRIVATE: may receive pushes creating children
 *             unpending must not yet touch it
 *             children can be created unlocked
 *             can change into other types
 *
 *             T_NEXUS only
 *
 * ST_CLOSED:  the request is done, no pushes can occur,
 *             unpending can proceed upwards
 *
 *             T_NEXUS only
 *
 * ST_UNPENDING: in the process of being pushed to the client
 *
 *	         T_DATA / T_CRC / T_SUBREQ
 *
 * ST_DELIVERED: We have pushed data up
 *
 *               any type
 *
 *		 for T_NEXUS, means "anything below is delivered"
 *
 * ST_PRUNED: nodes below have been freed, no attempt must
 *            be made to access any children of this node
 *            (similar to ST_PRIVATE)
 *
 *            T_NEXUS only
 */
enum n_state {
	ST_INVALID = 0,
	ST_DATA,
	ST_PRIVATE,
	ST_CLOSED,
	ST_UNPENDING,
	ST_DELIVERED,
	ST_PRUNED
} __attribute__ ((__packed__));

struct node_nexus {
	struct node_head	children;
	struct objcore		*oc;
	struct req		*req;

	/* number of nodes pending under this node while state == ST_PRIVATE */
	int			npending_private;
	/*
	 * crc for data nodes immediately below
	 * updated by push_crc, pretendgzip and gzgz
	 */
	struct nexus_gzip	gzip;	// in foreign
};

enum t_crc {
	INVALID = 0,
	GZIP_HDR,
	UPDATE,
	FINAL	// combine with parent or gen tail
} __attribute__ ((__packed__));

struct node_crc {
	enum t_crc	ctype;
	uint32_t	icrc;
	ssize_t		l_icrc;
};

struct node_data {
	const void		*ptr;
	struct stv_buffer	*stvbuf;
	size_t			len;
	enum vdp_action		act;
};

/*
 * we transfer the sub-request, boc and oc to the topreq thread, delivery
 * happens there
 */
struct node_subreq {
	struct req		*req;
	struct boc		*boc;
	// oc is NULL if already transferred back into req
	struct objcore		*oc;
	struct lock		*shared_lock;	// == &tree->nodes_lock
	pthread_cond_t		cond;
	// subreq to topreq delivery
	int			done;
};

enum n_alloc {
	NA_INVALID	= 0,
	NA_WS,
	NA_MPL
} __attribute__ ((__packed__));

struct node {					//		120b
	unsigned		magic;
#define NODE_MAGIC 0xe31edef3
	enum n_type		type;
	enum n_state		state;
	enum n_alloc		allocator;
	unsigned		is_end:1;
	VSTAILQ_ENTRY(node)	sibling;
	VSTAILQ_ENTRY(node)	unpend;
	struct node		*parent;

	union {
		struct node_nexus	nexus;	// T_NEXUS	72b
		struct node_data	data;	// T_DATA	32b
		struct node_subreq	subreq; // T_SUBREQ	88b
		struct node_crc	crc;	// T_CRC	16b
	};
};

/*
 * node mutation: turn an existing nexus into something else
 *
 * howto:
 *
 * 0) node must be T_NEXUS / ST_PRIVATE, untouched
 *
 * 1) call node_mutate_prep()
 *
 * 2) modify data in the destination-type specific struct
 *
 * 3) call node_mutate_lock() with destination type/state
 *
 * 4) optionally do more things under the lock
 *
 * 5) call node_mutate_unlock()
 *
 */

static inline void
node_mutate_prep(const struct bytes_tree *tree, struct node *node)
{
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	CHECK_OBJ_NOTNULL(node->parent, NODE_MAGIC);

	assert(node->state == ST_PRIVATE);
	assert(node->type == T_NEXUS);
	assert(node != tree->root);

	AZ(node->nexus.npending_private);
	AZ(node->nexus.oc);

	memset(&node->nexus, 0, sizeof node->nexus);
}

static inline void
node_mutate_lock(struct bytes_tree *tree, struct node *node,
		 enum n_type type, enum n_state state)
{
	/* these checks can be relexed when needed */
	assert(type == T_DATA ||
	       type == T_SUBREQ);
	assert(state == ST_DATA);

	Lck_Lock(&tree->tree_lock);
	node->type = type;
	node->state = state;
	if (node->parent->state != ST_PRIVATE)
		AZ(pthread_cond_signal(&tree->cond));
}

static inline void
node_mutate_unlock(struct bytes_tree *tree)
{
	Lck_Unlock(&tree->tree_lock);
}

//--------------

void node_fill_nodestock(struct ws *, struct node_head *);

//--------------

struct pesi;
struct node *node_new(struct pesi *pesi, struct node *parent,
    enum n_type type, enum n_state state);
void node_free(struct node **nodep);
void node_insert(const struct bytes_tree *, struct node *);
void set_closed(struct bytes_tree *, struct node *);

//--------------

void tree_prune(struct vdp_ctx *, struct node *);
void tree_free(struct vdp_ctx *, struct node *);
void tree_latch_error(struct bytes_tree *tree, int retval);

//--------------

void tree_deliver(struct vdp_ctx *, struct bytes_tree *);

/*-
 * Copyright 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Authors: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *	    Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * PESI per request state and task management
 */

/* for SSIZE_MAX */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include <string.h>

#include "cache/cache.h"

#include "VSC_pesi.h"

#include "pesi_tasks.h"
#include "pesi_flags.h"
#include "node_head.h"
#include "pesi.h"

/* shared with vmod code */
extern struct lock stats_lock;
extern struct VSC_pesi *stats;
struct lock stats_lock;
struct VSC_pesi *stats = NULL;

/* ------------------------------------------------------------
 * pesi tasks
 *
 * in the top request, we need to wait for all pesis to finish because, via
 * req->topreq and req->sp, they all reference the top request
 *
 * for this purpose, simple refcounting would suffice, but putting all the pesis
 * on a list does not add any relevant amount of complexity and might help with
 * troubleshooting and through additional assertions
 *
 * pesi_new always happens in a task but the destruction can happen in any
 * order:
 * - for T_SUBREQ, pesi outlives the task
 * - otherwise, pesi gets destroyed before the task
 */
struct pesi *
pesi_new(struct ws *ws, struct pesi_tasks *pesi_tasks)
{
	struct pesi *pesi;

	pesi = WS_Alloc(ws, sizeof *pesi);
	if (pesi == NULL)
		return (NULL);

	INIT_OBJ(pesi, PESI_MAGIC);
	pesi->pecx->magic = PECX_MAGIC;
	pesi->pesi_tasks = pesi_tasks;
	pesi->flags = PF_HAS_TASK | PF_CFG_DEFAULT;

	node_init_nodestock(&pesi->nodestock);

	Lck_Lock(&pesi_tasks->task_lock);
	VTAILQ_INSERT_TAIL(&pesi_tasks->task_head, pesi, list);
	assert(pesi_tasks->task_running >= 0);
	pesi_tasks->task_running++;
	Lck_Unlock(&pesi_tasks->task_lock);
	return (pesi);
}

/*
 * shutting down a pesi request is a two stage process, because Req_Cleanup() in
 * req_fini() called from vped_task() zeroes the workspace including our
 * per-request struct pesi, so removing the registration from
 * pesi_tasks->task_head need to happen before Req_Cleanup().
 *
 * But notification of the topreq needs to happen after Req_Cleanup(), because
 * it still references topreq and sp, thus the topreq may not finish until all
 * pesi tasks have finished.
 *
 * on the implementation:
 *
 * - in vdp_pesi_fini() we call pesi_destroy() to clean up the per-request
 *   struct pesi and remove it from the global list.
 *
 *   For possible debuggung purposes, we record the fact that a pesi request is
 *   finishing
 *
 * - in vped_task(), we call task_fini() to decrement the global counter and
 *   notify the top request
 *
 * for esi_level == 0, all cleanup happens in vdp_pesi_fini
 */

void
pesi_destroy(struct pesi **pesip)
{
	struct pesi *pesi;
	struct pesi_tasks *pesi_tasks;

	TAKE_OBJ_NOTNULL(pesi, pesip, PESI_MAGIC);
	CHECK_OBJ_NOTNULL(pesi->pecx, PECX_MAGIC);
	TAKE_OBJ_NOTNULL(pesi_tasks, &pesi->pesi_tasks, PESI_TREE_MAGIC);

	if (pesi->no_thread != 0) {
		Lck_Lock(&stats_lock);
		AN(stats);
		stats->no_thread += pesi->no_thread;
		Lck_Unlock(&stats_lock);
	}

	Lck_Lock(&pesi_tasks->task_lock);
	VTAILQ_REMOVE(&pesi_tasks->task_head, pesi, list);

	assert(pesi_tasks->task_running >= 0);

	if (pesi->flags & PF_HAS_TASK)
		pesi_tasks->task_finishing++;
	Lck_Unlock(&pesi_tasks->task_lock);

	memset(pesi, 0, sizeof *pesi);
}


void
task_fini(struct pesi_tasks *pesi_tasks, struct pesi *pesi)
{
	Lck_Lock(&pesi_tasks->task_lock);
	assert(pesi_tasks->task_running > 0);
	if (pesi == NULL) {
		assert(pesi_tasks->task_finishing > 0);
		pesi_tasks->task_finishing--;
	}
	else {
		AN(pesi->flags & PF_HAS_TASK);
		pesi->flags &= ~PF_HAS_TASK;
	}
	pesi_tasks->task_running--;
	if (pesi_tasks->task_running == 0) {
		AZ(pesi_tasks->task_finishing);
		AZ(pthread_cond_signal(&pesi_tasks->task_cond));
	}
	Lck_Unlock(&pesi_tasks->task_lock);
}

/*-
 * Copyright 2019 - 2021, 2023 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Authors: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *	    Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Implementation inspired by Varnish cache_esi_deliver.c
 */

/* for SSIZE_MAX */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include <stdlib.h>

#include "cache/cache_varnishd.h"
#include "cache/cache_transport.h"
#include "cache/cache_filter.h"
#include "cache/cache_vgz.h"
#include "cache/cache_esi.h"
#include "cache/cache_objhead.h"
#include "foreign/from_cache_esi_deliver.h"
#include "storage/storage.h"
#include "storage/storage_simple.h"
#include "vtim.h"
#include "vend.h"
#include "vgz.h"
#include "VSC_main.h"

#include "debug.h"
#include "vdp_pesi.h"
#include "node.h"
#include "node_assert.h"
#include "pesi.h"
#include "pesi_flags.h"
#include "pesi_tasks.h"

#include "misc.h"

/* Forward declarations */
static void vped_deliver(struct req *, struct boc *, int);
static void vped_reembark(struct worker *, struct req *);
static int vped_minimal_response(struct req *, uint16_t);

static const struct transport VPED_transport = {
	.magic =		TRANSPORT_MAGIC,
	.name =			"PESI_INCLUDE",
	.deliver =		vped_deliver,
	.reembark =		vped_reembark,
	.minimal_response =	vped_minimal_response,
};
static void vped_close_vdp(struct req *, int, const struct vdp *);
static void assert_vdp_next_not(struct req *, const struct vdp *);

/* shared with vmod code */
struct VSC_lck *lck_bytes_tree,
	*lck_bytes_nodes,
	*lck_pesi_tasks;
extern struct mempool *mempool;
struct mempool *mempool = NULL;

/* also used by our version of cache_esi_deliver.c */

/* ------------------------------------------------------------
 * push up data to the parent layer
 *
 * basically ved_ved / ved_bytes without the ecx
 */

static int v_matchproto_(vdp_fini_f)
vped_to_parent_fini(struct vdp_ctx *vdc, void **priv)
{
	(void)vdc;
	*priv = NULL;
	return (0);
}

static int v_matchproto_(vdp_bytes_f)
vped_to_parent_bytes(struct vdp_ctx *vdx, enum vdp_action act, void **priv,
    const void *ptr, ssize_t len)
{
	struct req *preq;

	CHECK_OBJ_NOTNULL(vdx, VDP_CTX_MAGIC);
	CAST_OBJ_NOTNULL(preq, *priv, REQ_MAGIC);
	if (act == VDP_END)
		act = VDP_FLUSH;
	return (VDP_bytes(preq->vdc, act, ptr, len));
}

static const struct vdp vped_to_parent = {
	.name =         "V2P",
	.bytes =        vped_to_parent_bytes,
	.fini =         vped_to_parent_fini,
};

/* ------------------------------------------------------------
 * process an esi task/request
 */
static void  v_matchproto_(task_func_t)
vped_task(struct worker *wrk, void *priv)
{
	struct req *req;
	struct sess *sp;
	struct pesi *pesi;
	struct pesi_tasks *pesi_tasks;
	struct node *node;
	enum req_fsm_nxt s;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);

	AZ(wrk->task->func);
	AZ(wrk->task->priv);

	/*
	 * if we got an aws reservation, we have been run via Pool_Task_Arg() or
	 * the direct call, otherwise via Pool_Task()
	 */
	if (wrk->aws->r != NULL) {
		priv = *(void **)priv;
		WS_Release(wrk->aws, 0);
	}
	CAST_OBJ_NOTNULL(req, priv, REQ_MAGIC);

	VSLdbgv(req, "vped_task: req=%p", req);

	assert(req->esi_level > 0);
	sp = req->sp;
	CHECK_OBJ_NOTNULL(sp, SESS_MAGIC);
	CAST_OBJ_NOTNULL(pesi, req->transport_priv, PESI_MAGIC);
	pesi_tasks = pesi->pesi_tasks;
	CHECK_OBJ_NOTNULL(pesi_tasks, PESI_TREE_MAGIC);
	pesi->wrk = wrk;
	node = pesi->node;
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	CHECK_OBJ_NOTNULL(pesi_tasks->bytes_tree, BYTES_TREE_MAGIC);

	THR_SetRequest(req);
	VSLb_ts_req(req, "Start", W_TIM_real(wrk));
	wrk->stats->esi_req++;

#ifdef DEBUG
	VSLdbgv(req, "vped_task: new req->vdc=%p nxt=%p", req->vdc,
		req->vdc->nxt);
	if (VTAILQ_EMPTY(&req->vdc->vdp))
		VSLdbg(req, "vped_task: VDP list empty");
	else {
		struct vdp_entry *vdpe = VTAILQ_LAST(&req->vdc->vdp,
						     vdp_entry_s);
		CHECK_OBJ_NOTNULL(vdpe, VDP_ENTRY_MAGIC);
		VSLdbgv(req, "vped_task: last VDP=%s", vdpe->vdp->name);
	}
#endif

	CNT_Embark(wrk, req);
	VCL_TaskEnter(req->privs);

	while (1) {
		req->wrk = wrk;
		pesi->woken = 0;
		VSLdbgv(req, "Starting CNT_Request() req=%p", req);
		s = CNT_Request(req);
		VSLdbgv(req, "CNT_Request()=%d req=%p", s, req);
		if (s == REQ_FSM_DONE)
			break;
		DSL(DBG_WAITINGLIST, req->vsl->wid,
		    "loop waiting for ESI (%d)", (int)s);
		assert(s == REQ_FSM_DISEMBARK);
		Lck_Lock(&req->sp->mtx);
		if (!pesi->woken) {
			VSLb(req->vsl, SLT_Debug, "Waiting for busyobj");
			(void)Lck_CondWait(
			    &wrk->cond, &req->sp->mtx);
			VSLb(req->vsl, SLT_Debug, "busyobj wakeup");
		}
		Lck_Unlock(&req->sp->mtx);
		pesi->woken = 0;
		AZ(req->wrk);
		VSLdbg(req, "Calling CNT_Embark()");
		CNT_Embark(wrk, req);
	}

	VCL_Rel(&req->vcl);

	switch (node->type) {
	case T_SUBREQ:
		assert(pesi == req->transport_priv);
		Lck_Lock(node->subreq.shared_lock);
		task_fini(pesi_tasks, pesi);
		node->subreq.done = 1;
		AZ(pthread_cond_signal(&node->subreq.cond));
		Lck_Unlock(node->subreq.shared_lock);
		break;
	case T_NEXUS:
		assert(node->nexus.req == req);
		assert(node->state == ST_PRIVATE);

		if (VSTAILQ_FIRST(&node->nexus.children) != NULL) {
			/*
			 * normal case: the nexus has received some children
			 *
			 * pesi was destroyed within the FSM via vdp_pesi_fini
			 */
			AZ(req->transport_priv);
			pesi = NULL;

			Lck_Lock(&pesi_tasks->bytes_tree->tree_lock);
			set_closed(pesi_tasks->bytes_tree, node);
			Lck_Unlock(&pesi_tasks->bytes_tree->tree_lock);
			AZ(pesi);
			task_fini(pesi_tasks, pesi);
			break;
		}

		/*
		 * we got nothing for this T_NEXUS:
		 *
		 * - get rid of the request
		 * - turn into a zero data node
		 *
		 * the req may or may not have been fini't in FSM, depending on
		 * error conditions. So we sanity check transport_priv and
		 * destroy pesi if needed
		 */

		// finish the request
		(void) VDP_Close(req->vdc, req->objcore, NULL);

		if (req->transport_priv != NULL)
			assert(req->transport_priv == pesi);

		pesi = req->transport_priv;
		req->transport_priv = NULL;

		if (pesi)
			pesi_destroy(&pesi);

		assert(req == node->nexus.req);

		req_fini(&node->nexus.req, wrk);

		/* turn into T_DATA node */
		node_mutate_prep(pesi_tasks->bytes_tree, node);
		memset(&node->data, 0, sizeof node->data);

		node_mutate_lock(pesi_tasks->bytes_tree, node,
				 T_DATA, ST_DATA);
		node_mutate_unlock(pesi_tasks->bytes_tree);

		task_fini(pesi_tasks, pesi);
		break;
	default:
		INCOMPL();
	}
}

/* ------------------------------------------------------------
 * from the parent, process an ESI include
 */

static int
vped_include(struct req *preq, const char *src, const char *host,
	     struct pesi *pesi, struct node *node, int gzip, int incl_cont)
{
	struct worker *wrk;
	struct sess *sp;
	struct pesi_tasks *pesi_tasks;
	struct req *req;
	struct bytes_tree *tree;
	struct pesi *pesi2;

	VSLdbg(preq, "vped_include: enter");
	CHECK_OBJ_NOTNULL(preq, REQ_MAGIC);
	CHECK_OBJ_NOTNULL(preq->top, REQTOP_MAGIC);
	sp = preq->sp;
	CHECK_OBJ_NOTNULL(sp, SESS_MAGIC);
	CHECK_OBJ_NOTNULL(pesi, PESI_MAGIC);
	pesi_tasks = pesi->pesi_tasks;
	CHECK_OBJ_NOTNULL(pesi_tasks, PESI_TREE_MAGIC);
	tree = pesi_tasks->bytes_tree;
	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	wrk = preq->wrk;

	if ((unsigned)preq->esi_level >= cache_param->max_esi_depth) {
		VSLb(preq->vsl, SLT_VCL_Error,
		    "ESI depth limit reached (param max_esi_depth = %u)",
		    cache_param->max_esi_depth);
		return (-1);
	}

	req = Req_New(sp);
	AN(req);
	VSLdbgv(preq, "vped_include: new req=%p", req);
	assert(VXID(req->vsl->wid) == VXID(NO_VXID));

	pesi2 = pesi_new(req->ws, pesi_tasks);
	if (pesi2 == NULL) {
		VSLb(req->vsl, SLT_Error,
		     "Cannot allocate workspace for parallel ESI data");
		req_fini(&req, wrk);
		tree_latch_error(tree, -1);
		return (-1);
	}
	pesi2->pecx->incl_cont = incl_cont;

	req->vsl->wid = VXID_Get(wrk, VSL_CLIENTMARKER);
	VSLdbgv(preq, "vped_include: new xid=%ju", VXID(req->vsl->wid));

	VSLb(req->vsl, SLT_Begin, "req %ju esi", VXID(preq->vsl->wid));
	VSLb(preq->vsl, SLT_Link, "req %ju esi", VXID(req->vsl->wid));

	req->esi_level = preq->esi_level + 1;

	req->top = preq->top;

	HTTP_Setup(req->http, req->ws, req->vsl, SLT_ReqMethod);
	HTTP_Dup(req->http, preq->http0);

	http_SetH(req->http, HTTP_HDR_URL, WS_Copy(req->ws, src, -1));
	if (host != NULL && *host != '\0')  {
		http_Unset(req->http, H_Host);
		http_SetHeader(req->http, host);
	}

	http_ForceField(req->http, HTTP_HDR_METHOD, "GET");
	http_ForceField(req->http, HTTP_HDR_PROTO, "HTTP/1.1");

	/* Don't allow conditionals, we can't use a 304 */
	http_Unset(req->http, H_If_Modified_Since);
	http_Unset(req->http, H_If_None_Match);

	/* Don't allow Range */
	http_Unset(req->http, H_Range);

	/* Set Accept-Encoding according to what we want */
	if (gzip)
		http_ForceHeader(req->http, H_Accept_Encoding, "gzip");
	else
		http_Unset(req->http, H_Accept_Encoding);

	/* Client content already taken care of */
	http_Unset(req->http, H_Content_Length);
	req->req_body_status = BS_NONE;

	AZ(req->vcl);
	if (req->top->vcl0)
		req->vcl = req->top->vcl0;
	else
		req->vcl = preq->vcl;
	VCL_Ref(req->vcl);

	req->req_step = R_STP_TRANSPORT;
	req->t_req = preq->t_req;

#ifdef DEBUG
	VSLdbgv(preq, "vped_include: preq->vdc=%p nxt=%p", preq->vdc,
		preq->vdc->nxt);
	if (VTAILQ_EMPTY(&preq->vdc->vdp))
		VSLdbg(preq, "vped_include: preq VDP list empty");
	else if (req->esi_level > 1) {
		struct vdp_entry *vdpe = VTAILQ_LAST(&preq->vdc->vdp,
						     vdp_entry_s);
		CHECK_OBJ_NOTNULL(vdpe, VDP_ENTRY_MAGIC);
		VSLdbgv(preq, "vped_include: preq last VDP=%s", vdpe->vdp->name);
	}
#endif

	req->transport = &VPED_transport;

	pesi2->node = node;

	req->transport_priv = pesi2;

	req->ws_req = WS_Snapshot(req->ws);

	assert(node->type == T_NEXUS);
	node->nexus.req = req;

	VSLdbgv(preq, "vped_include: attempt new thread req=%p", req);

	if (pesi->flags & PF_CFG_SERIAL) {
		vped_task(wrk, req);
		return (0);
	}

	if (pesi->flags & PF_CFG_THREAD) {
		req->task->func = vped_task;
		req->task->priv = req;
		AZ(Pool_Task(wrk->pool, req->task, TASK_QUEUE_RUSH));
		return (1);
	}

	if (Pool_Task_Arg(wrk, TASK_QUEUE_REQ, vped_task, &req, sizeof req) == 1)
		return (1);

	pesi->no_thread++;
	VSLb(preq->vsl, SLT_Error, "vdp pesi: No thread available "
	     "for ESI subrequest %ju, continuing in serial",
	     VXID(req->vsl->wid));

	/*
	 * we can not use the self-rescheduling facility of Pool_Task_Arg
	 * because we cannot unschedule ourself
	 */
	wrk->task->func = NULL;
	wrk->task->priv = NULL;
	WS_Release(wrk->aws, 0);
	vped_task(wrk, req);
	return (0);
}

/* ------------------------------------------------------------
 * buffer/reference vdp bytes pushed from the pesi vdp
 * for literal segments inbetween includes
 */

static int v_matchproto_(vdp_init_f)
pesi_buf_init(VRT_CTX, struct vdp_ctx *vdc, void **priv)
{
	struct req *req;
	struct pesi *pesi;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vdc, VDP_CTX_MAGIC);
	req = ctx->req;
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	CAST_OBJ_NOTNULL(pesi, *priv, PESI_MAGIC);

	assert(WS_Allocated(req->ws, pesi, sizeof *pesi));
	node_fill_nodestock(req->ws, &pesi->nodestock);

	return (0);
}

static int v_matchproto_(vdp_fini_f)
pesi_buf_fini(struct vdp_ctx *vdc, void **priv)
{
	(void)vdc;

	*priv = NULL;
	return (0);
}

static int v_matchproto_(vdp_bytes_f)
pesi_buf_bytes(struct vdp_ctx *vdx, enum vdp_action act, void **priv,
    const void *ptr, ssize_t len)
{
	struct req *req;
	struct bytes_tree *tree;
	struct node *node, *parent;
	struct pesi *pesi;
	void *p;
	size_t l;
	unsigned refok;

	CAST_OBJ_NOTNULL(pesi, *priv, PESI_MAGIC);
	CHECK_OBJ_NOTNULL(pesi->pesi_tasks, PESI_TREE_MAGIC);
	tree = pesi->pesi_tasks->bytes_tree;
	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);

	if (tree->retval || ptr == NULL || len == 0)
		return (tree->retval);

	CHECK_OBJ_NOTNULL(vdx, VDP_CTX_MAGIC);

	parent = pesi->node;
	CHECK_OBJ_NOTNULL(parent, NODE_MAGIC);
	assert(parent->type == T_NEXUS);
	VSLdbgv(vdx, "bytes_add: parent=%p front=%p",
		parent, tree->front);

	req = parent->nexus.req;
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);

	VSLdbg(vdx, "bytes_add: adding data to node");
	node = node_new(pesi, parent, T_DATA, ST_DATA);
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	node->data.len = len;
	node->data.act = act;

	/*
	 * non-api knowledge from VDP_DeliverObj()
	 *  final = req->objcore->flags & (OC_F_PRIVATE | OC_F_HFM | OC_F_HFP);
	 *
	 * as long as we keep a reference on a non-final object, it will
	 * not go away, so we can keep a reference only and avoid copying
	 *
	 * XXX TODO: Add a way in varnish-cache to veto the final flag to
	 * ObjIterate() ?
	 *
	 */
	if (parent->nexus.oc == NULL &&
	    (req->objcore->flags & OC_F_FINAL) == 0) {

		parent->nexus.oc = req->objcore;
		HSH_Ref(parent->nexus.oc);
	}

	refok = (parent->nexus.oc != NULL);

	AN(ptr);
	AZ(node->data.ptr);
	if (refok) {
		VSLdbg(vdx, "bytes_add: stv ref");
		node->data.ptr = ptr;
	}
	else {
		AN(req->objcore->flags & OC_F_FINAL);
		VSLdbg(vdx, "bytes_add: allocating storage buffer");
		node->data.stvbuf = STV_AllocBuf(vdx->wrk, stv_transient, len);
		if (node->data.stvbuf == NULL) {
			VSLb(vdx->vsl, SLT_Error, "Cannot allocate transient "
			     "storage to buffer response data while waiting "
			     "for parallel ESI");
			VSLdbg(vdx, "bytes_add: exit after transient alloc "
			       "failure");

			tree_latch_error(tree, -1);
			node_free(&node);

			return (tree->retval);
		}

		node->data.act = VDP_NULL;
		p = STV_GetBufPtr(node->data.stvbuf, &l);
		assert(l == (size_t)len);
		memcpy(p, ptr, len);
	}

	node_insert(tree, node);

	VSLdbgv(vdx, "bytes_add to %s parent: exit",
	    parent->state == ST_PRIVATE ? "private" : "open");

	return (tree->retval);
}

static const struct vdp VDP_pesi_buf = {
	.name =		"pesi_buf",
	.init =		pesi_buf_init,
	.bytes =	pesi_buf_bytes,
	.fini =		pesi_buf_fini,
};

/* ------------------------------------------------------------
 * init/fini for sub-structures
 */

static void
bytes_tree_init(struct bytes_tree *bytes_tree, struct node *root_node)
{

	AN(bytes_tree);
	INIT_OBJ(bytes_tree, BYTES_TREE_MAGIC);

	Lck_New(&bytes_tree->tree_lock, lck_bytes_tree);
	Lck_New(&bytes_tree->nodes_lock, lck_bytes_nodes);
	AZ(pthread_cond_init(&bytes_tree->cond, NULL));
	AZ(bytes_tree->retval);
	AZ(bytes_tree->npending);

	bytes_tree->front = root_node;
	bytes_tree->root = root_node;
}

static void
bytes_tree_fini(struct bytes_tree *bytes_tree, struct vdp_ctx *vdc)
{

	CHECK_OBJ_NOTNULL(bytes_tree, BYTES_TREE_MAGIC);

	/*
	 * prevent race with vped_task()
	 */
	Lck_Lock(&bytes_tree->tree_lock);
	assert_node(bytes_tree->root, CHK_ANY);
	if (bytes_tree->root->nexus.npending_private == 1 &&
	    bytes_tree->npending == 0) {
		AZ(bytes_tree->retval);
		assert(bytes_tree->root->state == ST_PRIVATE);
		assert(bytes_tree->root->type == T_NEXUS);
	} else if (bytes_tree->retval == 0) {
		assert(bytes_tree->root->nexus.npending_private == 0);
		assert(bytes_tree->npending == 0);
		assert_node(bytes_tree->root, CHK_DELI);
	}
	/*
	 * else we got an error condition and cannot make assumptions about the
	 * state of the tree before all tasks have finished
	 */

	/*
	 * for an early client close, our subrequests had no chance to run
	 */
	tree_prune(vdc, bytes_tree->root);

	Lck_Unlock(&bytes_tree->tree_lock);
	Lck_Delete(&bytes_tree->tree_lock);
	AZ(pthread_cond_destroy(&bytes_tree->cond));

	tree_free(vdc, bytes_tree->root);
	Lck_Delete(&bytes_tree->nodes_lock);
}

static struct node *
root_node_new(struct pesi *pesi, struct req *req)
{
	struct node *root_node;

	root_node = node_new(pesi, NULL, T_NEXUS, ST_PRIVATE);
	CHECK_OBJ_NOTNULL(root_node, NODE_MAGIC);
	root_node->nexus.req = req;
	root_node->nexus.npending_private = 1;

	return (root_node);
}

static int
init_err(struct vsl_log *vsl, const char *s)
{

	VSLb(vsl, SLT_Error, "%s", s);
	return (-1);
}

/* ------------------------------------------------------------
 * process ESI objects from storage
 */

static int v_matchproto_(vdp_init_f)
vdp_pesi_init(VRT_CTX, struct vdp_ctx *vdc, void **priv)
{
	struct req *req;
	struct pesi *pesi;
	struct pecx *pecx;
	struct pesi_tasks *pesi_tasks;
	struct node *root_node;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_ORNULL(ctx->req, REQ_MAGIC);
	CHECK_OBJ_NOTNULL(vdc, VDP_CTX_MAGIC);
	CHECK_OBJ_ORNULL(vdc->oc, OBJCORE_MAGIC);
	CHECK_OBJ_NOTNULL(vdc->hp, HTTP_MAGIC);
	AN(vdc->clen);

	AN(priv);
	AZ(*priv);

	if (vdc->oc == NULL || !ObjHasAttr(vdc->wrk, vdc->oc, OA_ESIDATA))
		return (1);

	req = ctx->req;
	if (req == NULL) {
		VSLb(vdc->vsl, SLT_Error,
		     "esi can only be used on the client side");
		return (1);
	}

	RFC2616_Weaken_Etag(vdc->hp);
	req->res_mode |= RES_ESI;
	if (*vdc->clen != 0)
		*vdc->clen = -1;

	if (req->esi_level > 0) {
		CAST_OBJ_NOTNULL(pesi, req->transport_priv, PESI_MAGIC);
		*priv = pesi;

		assert(WS_Allocated(req->ws, pesi, sizeof *pesi));
		node_fill_nodestock(req->ws, &pesi->nodestock);
		get_task_cfg(ctx, &pesi->flags);
#ifdef DEBUG_PESI_WS
		pesi->ws_snap = WS_Snapshot(req->ws);
#endif
		if (VDP_Push(ctx, req->vdc, req->ws, &VDP_pesi_buf, pesi)) {
			return (init_err(req->vsl, "VDP_Push(VDP_pesi_buf) failed "
			    "(likely insufficient workspace)"));
		}
		return (0);
	}

	AZ(req->esi_level);

	if ((pesi_tasks = WS_Alloc(req->ws, sizeof(*pesi_tasks))) == NULL) {
		return (init_err(req->vsl, "insufficient workspace for "
		     "global parallel ESI data"));
	}

	INIT_OBJ(pesi_tasks, PESI_TREE_MAGIC);

	if ((pesi_tasks->bytes_tree = WS_Alloc(req->ws, sizeof(*pesi_tasks->bytes_tree)))
	    == NULL) {
		return (init_err(req->vsl, "insufficient workspace for "
		     "parallel ESI tree data"));
	}

	Lck_New(&pesi_tasks->task_lock, lck_pesi_tasks);
	AZ(pthread_cond_init(&pesi_tasks->task_cond, NULL));
	VTAILQ_INIT(&pesi_tasks->task_head);
	AZ(pesi_tasks->task_running);

	pesi = pesi_new(req->ws, pesi_tasks);
	if (pesi == NULL) {
		return (init_err(req->vsl, 
		     "Cannot allocate workspace for parallel ESI data"));
	}
	get_task_cfg(ctx, &pesi->flags);
	pecx = pesi->pecx;
	*priv = pesi;

	node_fill_nodestock(req->ws, &pesi->nodestock);

	root_node = root_node_new(pesi, req);

	bytes_tree_init(pesi_tasks->bytes_tree, root_node);

	pesi->node = root_node;
	AZ(pecx->state);
	AZ(pesi->woken);

	if (VDP_Push(ctx, req->vdc, req->ws, &VDP_pesi_buf, pesi)) {
		return (init_err(req->vsl, "VDP_Push(VDP_pesi_buf) failed "
		    "(likely insufficient workspace)"));
	}
#ifdef DEBUG_PESI_WS
	pesi->ws_snap = WS_Snapshot(req->ws);
#endif
	return (0);
}

static int v_matchproto_(vdp_fini_f)
vdp_pesi_fini(struct vdp_ctx *vdc, void **priv)
{
	struct req *req;
	struct pesi *pesi;
	struct pesi_tasks *pesi_tasks;
	struct node *node;

	CHECK_OBJ_NOTNULL(vdc, VDP_CTX_MAGIC);
	AN(priv);

	CAST_OBJ_NOTNULL(pesi, *priv, PESI_MAGIC);
	pesi_tasks = pesi->pesi_tasks;
	CHECK_OBJ_NOTNULL(pesi_tasks, PESI_TREE_MAGIC);

	node = pesi->node;
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);

	assert(node->type == T_NEXUS);
	req = node->nexus.req;
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);

	VSLdbgv(req, "vdp_pesi_fini: esi_level=%u", req->esi_level);

#ifdef DEBUG_PESI_WS
	VSL(SLT_Debug, 0, "ws used %u free %u",
	    pdiff((void *)pesi->ws_snap, (void *)WS_Snapshot(req->ws)),
	    pdiff(req->ws->f, req->ws->e));
	{
		unsigned i = 0;
		struct node *c;
		VSTAILQ_FOREACH(c, &node->nexus.children, sibling)
			i++;
		VSL(SLT_Debug, 0, "nodes used %u", i);
	}
#endif

	if (req->esi_level > 0) {
		assert(req->transport_priv == pesi);
		*priv = NULL;
		req->transport_priv = NULL;

		pesi_destroy(&pesi);

		return (0);
	}

	assert(req->transport_priv == NULL ||
	       *(unsigned *)req->transport_priv != PESI_MAGIC);

	CHECK_OBJ_NOTNULL(pesi_tasks, PESI_TREE_MAGIC);
	assert(pesi_tasks == pesi->pesi_tasks);

	pesi_destroy(&pesi);
	task_fini(pesi_tasks, pesi);

	/*
	 * ensure that all tasks are done
	 *
	 * there can be at most one signal ever being posted on task_cond, so,
	 * deliberately, this is if (...) not while (...)
	 */
	if (pesi_tasks->task_running > 0) {
		Lck_Lock(&pesi_tasks->task_lock);
		if (pesi_tasks->task_running > 0)
			AZ(Lck_CondWait(&pesi_tasks->task_cond,
					&pesi_tasks->task_lock));
		Lck_Unlock(&pesi_tasks->task_lock);
	}

	bytes_tree_fini(pesi_tasks->bytes_tree, req->vdc);

	AZ(pesi_tasks->task_running);
	assert(VTAILQ_EMPTY(&pesi_tasks->task_head));
	Lck_Delete(&pesi_tasks->task_lock);
	AZ(pthread_cond_destroy(&pesi_tasks->task_cond));

	*priv = NULL;
	return (0);
}

static ssize_t
vped_decode_len(struct req *req, const uint8_t **pp)
{
	const uint8_t *p;
	ssize_t l;

	p = *pp;
	switch (*p & 15) {
	case 1:
		l = p[1];
		p += 2;
		break;
	case 2:
		l = vbe16dec(p + 1);
		p += 3;
		break;
	case 8:
		l = vbe64dec(p + 1);
		p += 9;
		break;
	default:
		VSLb(req->vsl, SLT_Error,
		    "ESI-corruption: Illegal Length %d %d\n", *p, (*p & 15));
		WRONG("ESI-codes: illegal length");
	}
	*pp = p;
	assert(l > 0);
	return (l);
}

static int v_matchproto_(vdp_bytes_f)
vdp_pesi_bytes(struct vdp_ctx *vdx, enum vdp_action act, void **priv,
    const void *ptr, ssize_t len)
{
	struct req *req;
	uint8_t *q, *r;
	ssize_t l = 0;
	const uint8_t *pp;
	struct pesi *pesi;
	struct pecx *pecx;
	struct nexus_gzip *parent_gzip = NULL;
	struct bytes_tree *tree;
	struct node *node, *child = NULL;
	int incl_cont, incl_cont_override, retval = 0, parallel;

	CHECK_OBJ_NOTNULL(vdx, VDP_CTX_MAGIC);

	if (act == VDP_END)
		act = VDP_FLUSH;

	AN(priv);
	CAST_OBJ_NOTNULL(pesi, *priv, PESI_MAGIC);
	CHECK_OBJ_NOTNULL(pesi->pecx, PECX_MAGIC);
	CHECK_OBJ_NOTNULL(pesi->pesi_tasks, PESI_TREE_MAGIC);
	CHECK_OBJ_NOTNULL(pesi->pesi_tasks->bytes_tree, BYTES_TREE_MAGIC);
	pecx = pesi->pecx;
	tree = pesi->pesi_tasks->bytes_tree;

	node = pesi->node;
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);

	assert(node->type == T_NEXUS);
	req = node->nexus.req;
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);

	/*
	 * more data getting pushed to us after ESI level 0 processing has
	 * finished
	 *
	 * we would want to remove ourselves from the VDP list, but cannot, as
	 * that would trigger our _fini while we are in _bytes - catch22...
	 *
	 */
	if (req->esi_level == 0 &&
	    node->state >= ST_DELIVERED &&
	    pecx->state == 99) {
		return (tree->retval);
	}

	assert(node->state == ST_PRIVATE);

	assert(ObjHasAttr(req->wrk, req->objcore, OA_ESIDATA));

	if (node->parent != NULL && node->parent->nexus.gzip.is)
		parent_gzip = &node->parent->nexus.gzip;

	incl_cont_override = (pesi->flags & PF_CFG_ONERROR_CONTINUE) != 0;

	pp = ptr;
	while (retval == 0 && tree->retval == 0) {
		incl_cont = 0;
		switch (pecx->state) {
		case 0:
			pecx->p = ObjGetAttr(req->wrk, req->objcore,
			    OA_ESIDATA, &l);
			AN(pecx->p);
			assert(l > 0);
			pecx->e = pecx->p + l;

			if (*pecx->p == VEC_GZ) {
				if (parent_gzip == NULL) {
					AZ(child);
					child = node_new(pesi, node,
					    T_CRC, ST_DATA);
					CHECK_OBJ_NOTNULL(child, NODE_MAGIC);
					child->crc.ctype = GZIP_HDR;
					node_insert(tree, child);
					child = NULL;
				}
				node->nexus.gzip.is = 1;
				node->nexus.gzip.crc = crc32(0L, Z_NULL, 0);
				AZ(node->nexus.gzip.l_crc);
				node->nexus.gzip.up = parent_gzip;

				pecx->p++;
			}
			pecx->state = 1;
			break;
		case 1:
			if (pecx->p >= pecx->e) {
				pecx->state = 2;
				break;
			}
			switch (*pecx->p) {
			case VEC_V1:
			case VEC_V2:
			case VEC_V8:
				pecx->l = vped_decode_len(req, &pecx->p);
				if (pecx->l < 0)
					return (-1);
				Debug("VEC_V(%d)\n", (int)pecx->l);
				if (node->nexus.gzip.is) {
					assert(*pecx->p == VEC_C1 ||
					    *pecx->p == VEC_C2 ||
					    *pecx->p == VEC_C8);
					l = vped_decode_len(req, &pecx->p);
					if (l < 0)
						return (-1);

					AZ(child);
					child = node_new(pesi, node,
					    T_CRC, ST_DATA);
					CHECK_OBJ_NOTNULL(child, NODE_MAGIC);
					child->crc.ctype = UPDATE;
					child->crc.icrc = vbe32dec(pecx->p);
					child->crc.l_icrc = l;
					node_insert(tree, child);
					child = NULL;

					pecx->p += 4;
				}
				pecx->state = 3;
				break;
			case VEC_S1:
			case VEC_S2:
			case VEC_S8:
				pecx->l = vped_decode_len(req, &pecx->p);
				if (pecx->l < 0)
					return (-1);
				Debug("SKIP1(%d)\n", (int)pecx->l);
				pecx->state = 4;
				break;
			case VEC_IC:
				incl_cont =
					FEATURE(FEATURE_ESI_INCLUDE_ONERROR);
				/* FALLTHROUGH */
			case VEC_IA:
				pecx->p++;
				q = (void*)strchr((const char*)pecx->p, '\0');
				AN(q);
				q++;
				r = (void*)strchr((const char*)q, '\0');
				AN(r);
				Debug("INCL [%s][%s] BEGIN\n", q, pecx->p);

				AZ(child);
				child = node_new(pesi, node,
				    T_NEXUS, ST_PRIVATE);
				CHECK_OBJ_NOTNULL(child, NODE_MAGIC);

				incl_cont |= incl_cont_override;

				VSLdbgv(vdx, "vped_vdp: call vped_include "
				       "incl_cont=%d", incl_cont);
				parallel =
					vped_include(req, (const char*)q,
						     (const char*)pecx->p,
						     pesi, child,
						     node->nexus.gzip.is,
						     incl_cont
						);
				VSLdbgv(vdx, "vped_vdp: vped_include()=%d",
					parallel);
				if (parallel >= 0) {
					node_insert(tree, child);
					child = NULL;
				}
				else if (incl_cont == 0)
					tree_latch_error(tree, -1);
				if (child != NULL)
					node_free(&child);

				AZ(child);
				Debug("INCL [%s][%s] END\n", q, pecx->p);
				pecx->p = r + 1;
				break;
			default:
				VSLb(vdx->vsl, SLT_Error,
				    "ESI corruption line %d 0x%02x [%s]\n",
				    __LINE__, *pecx->p, pecx->p);
				WRONG("ESI-codes: Illegal code");
			}
			break;
		case 2:
			if (node->nexus.gzip.is) {
				AZ(child);
				child = node_new(pesi, node, T_CRC, ST_DATA);
				CHECK_OBJ_NOTNULL(child, NODE_MAGIC);
				child->crc.ctype = FINAL;
				node_insert(tree, child);
				child = NULL;
			}
			assert(pecx->p >= pecx->e);

			pecx->state = 99;
			break;
		case 3:
		case 4:
			/*
			 * There is no guarantee that the 'l' bytes are all
			 * in the same storage segment, so loop over storage
			 * until we have processed them all.
			 */
			VSLdbgv(vdx, "vped_vdp: pecx->state=%d", pecx->state);
			if (pecx->l <= len) {
				if (pecx->state == 3)
					retval = VDP_bytes(vdx, act, pp, pecx->l);
				len -= pecx->l;
				pp += pecx->l;
				pecx->state = 1;
				break;
			}
			if (pecx->state == 3 && len > 0)
				retval = VDP_bytes(vdx, act, pp, len);
			pecx->l -= len;
			if (retval)
				break;
			return (0);
		case 99:
			/*
			 * VEP does not account for the PAD+CRC+LEN
			 * so we can see up to approx 15 bytes here.
			 */

			if (req->esi_level > 0)
				return (tree->retval);

			/*
			 * we need to wait for completion here because at vdp
			 * fini time, the V1D is already closed
			 */
			Lck_Lock(&tree->tree_lock);
			set_closed(tree, node);

			assert_vdp_next_not(req, &VDP_pesi);
			vped_close_vdp(req, 1, &VDP_pesi_buf);
			assert_vdp_next_not(req, &VDP_pesi);

			tree_deliver(vdx, tree);
			while (!tree->retval
			       && (tree->root->state < ST_DELIVERED ||
				   tree->npending > 0)) {
				VSLdbgv(vdx, "vdp_pesi_bytes: waiting - "
					"state=%d npending=%d",
					tree->root->state,
					tree->npending);
				AZ(Lck_CondWait(&tree->cond,
						&tree->tree_lock));
				tree_deliver(vdx, tree);
			}
			if (! tree->end_sent)
				(void) VDP_bytes(vdx, VDP_END, NULL, 0);
			Lck_Unlock(&tree->tree_lock);

			return (tree->retval);
		default:
			WRONG("FOO");
			break;
		}
	}
	if (retval != 0)
		tree_latch_error(tree, retval);
	return (tree->retval);
}

const struct vdp VDP_pesi = {
	.name =		"pesi",
	.init =		vdp_pesi_init,
	.bytes =	vdp_pesi_bytes,
	.fini =		vdp_pesi_fini,
};

/* ------------------------------------------------------------
 * transport interface
 */

//LCOV_EXCL_START
static int v_matchproto_(vtr_minimal_response_f)
vped_minimal_response(struct req *req, uint16_t status)
{
	(void)req;
	(void)status;
	WRONG("esi:includes should not try minimal responses");
}
//LCOV_EXCL_STOP

static void v_matchproto_(vtr_reembark_f)
vped_reembark(struct worker *wrk, struct req *req)
{
	struct pesi *pesi;

	(void)wrk;
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	CAST_OBJ_NOTNULL(pesi, req->transport_priv, PESI_MAGIC);
	CHECK_OBJ_NOTNULL(pesi->pecx, PECX_MAGIC);

	VSLb(req->vsl, SLT_Debug, "reembark wakeup");
	Lck_Lock(&req->sp->mtx);
	pesi->woken = 1;
	AZ(pthread_cond_signal(&pesi->wrk->cond));
	Lck_Unlock(&req->sp->mtx);
}

static void
assert_vdp_next_not(struct req *req, const struct vdp *vdp)
{
	struct vdp_entry *nxt;
	struct vdp_ctx *vdc;

	vdc = req->vdc;
	nxt = vdc->nxt;

	CHECK_OBJ_NOTNULL(nxt, VDP_ENTRY_MAGIC);
	assert(nxt->vdp != vdp);
}

/*
 * close vdp at position skip and assert it has the right type
 *
 * there must be at least one vdp remaining
 */
static void
vped_close_vdp(struct req *req, int skip, const struct vdp *vdp)
{
	struct vdp_entry *vdpe, *nxt;
	struct vdp_ctx *vdc;

	vdc = req->vdc;
	nxt = vdc->nxt;
	vdpe = VTAILQ_FIRST(&vdc->vdp);

	/*
	 * it is a caller error to skip too many vdps, so if any of these checks
	 * fail, know your stack...
	 */
	while (skip-- > 0) {
		CHECK_OBJ_NOTNULL(vdpe, VDP_ENTRY_MAGIC);
		vdpe = VTAILQ_NEXT(vdpe, list);
	}
	CHECK_OBJ_NOTNULL(vdpe, VDP_ENTRY_MAGIC);

	assert(vdpe->vdp == vdp);
	AN(vdpe->vdp->fini);
	AZ(vdpe->vdp->fini(vdc, &vdpe->priv));
	AZ(vdpe->priv);
	if (nxt == vdpe)
		nxt = VTAILQ_NEXT(vdpe, list);
	VTAILQ_REMOVE(&vdc->vdp, vdpe, list);

	if (nxt == NULL)
		nxt = VTAILQ_FIRST(&vdc->vdp);
	CHECK_OBJ_NOTNULL(nxt, VDP_ENTRY_MAGIC);
	vdc->nxt = nxt;
}

static void
push_vdps(VRT_CTX, struct req *req, struct vped_gzgz_priv *vgzgz,
    struct nexus_gzip *gz)
{
	if (vgzgz != NULL) {
		AZ(gz);
		AZ(VDP_Push(ctx, req->vdc, req->ws, &vped_gzgz, vgzgz));
	}
	else if (gz != NULL) {
		AZ(vgzgz);
		AZ(VDP_Push(ctx, req->vdc, req->ws, &vped_pretend_gz, gz));
	}
	else {
		AZ(vgzgz);
		AZ(gz);
	}
	return;
}

static void v_matchproto_(vtr_deliver_f)
vped_deliver(struct req *req, struct boc *boc, int wantbody)
{
	int obj_gzipped, obj_esi;
	struct pesi *pesi;
	struct bytes_tree *tree;
	struct node *node, *parent;
	struct nexus_gzip *gz;
	struct vped_gzgz_priv *vgzgz = NULL;
	struct vrt_ctx ctx[1];
	uint16_t status;

	VSLdbgv(req, "vped_deliver: req=%p boc=%p wantbody=%d", req, boc,
		wantbody);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	CHECK_OBJ_ORNULL(boc, BOC_MAGIC);
	CHECK_OBJ_NOTNULL(req->objcore, OBJCORE_MAGIC);

	CAST_OBJ_NOTNULL(pesi, req->transport_priv, PESI_MAGIC);
	CHECK_OBJ_NOTNULL(pesi->pesi_tasks, PESI_TREE_MAGIC);
	CHECK_OBJ_NOTNULL(pesi->pesi_tasks->bytes_tree, BYTES_TREE_MAGIC);
	tree = pesi->pesi_tasks->bytes_tree;

	node = pesi->node;
	parent = node->parent;
	CHECK_OBJ_NOTNULL(parent, NODE_MAGIC);

	VSLdbgv(req, "vped_deliver: req=%p boc=%p wantbody=%d incl_cont=%d",
		req, boc, wantbody, pesi->pecx->incl_cont);
	assert(parent->type == T_NEXUS);

	status = req->resp->status % 1000;
	if (!pesi->pecx->incl_cont && status != 200 && status != 204) {
		tree_latch_error(tree, -1);
		(void) VDP_Close(req->vdc, req->objcore, boc);
		return;
	}

	if (wantbody == 0) {
		(void) VDP_Close(req->vdc, req->objcore, boc);
		return;
	}

	VSLdbgv(req, "vped_deliver: ObjGetLen=%lu",
		ObjGetLen(req->wrk, req->objcore));
	if (boc == NULL && ObjGetLen(req->wrk, req->objcore) == 0) {
		(void) VDP_Close(req->vdc, req->objcore, boc);
		return;
	}

	obj_gzipped = ObjCheckFlag(req->wrk, req->objcore, OF_GZIPED);
	obj_esi = ObjHasAttr(req->wrk, req->objcore, OA_ESIDATA);

	gz = &parent->nexus.gzip;

	INIT_OBJ(ctx, VRT_CTX_MAGIC);
	VCL_Req2Ctx(ctx, req);

#ifndef ISSUE19_RELIEF
	// #19 disable streaming for now
	if (boc != NULL)
		ObjWaitState(req->objcore, BOS_FINISHED);
	if (req->objcore->flags & OC_F_FAILED) {
		if (!pesi->pecx->incl_cont)
			tree_latch_error(tree, -1);
		return;
	}
#endif

	if (gz->is && obj_gzipped && !(req->res_mode & RES_ESI)) {
		/* OA_GZIPBITS are not valid until BOS_FINISHED */
		if (boc != NULL)
			ObjWaitState(req->objcore, BOS_FINISHED);

		if (req->objcore->flags & OC_F_FAILED) {
			if (!pesi->pecx->incl_cont)
				tree_latch_error(tree, -1);
			(void) VDP_Close(req->vdc, req->objcore, boc);
			return;
		}

		vgzgz = WS_Alloc(req->ws, sizeof *vgzgz);
		if (vgzgz == NULL) {
			VSLb(req->vsl, SLT_Error,
			     "Insufficient workspace for ESI gzip data");
			if (!pesi->pecx->incl_cont)
				tree_latch_error(tree, -1);
			(void) VDP_Close(req->vdc, req->objcore, boc);
			return;
		}
		INIT_OBJ(vgzgz, VPED_GZGZ_PRIV_MAGIC);
		vgzgz->gz = gz;
		vgzgz->objcore = req->objcore;
		gz = NULL;
	}
	else if (gz->is && !obj_gzipped) {
		AZ(vgzgz);
		AN(gz);
	}
	else {
		AZ(vgzgz);
		gz = NULL;
	}

	if (obj_esi && req->res_mode & RES_ESI) {
		VSLdbg(req, "vped_deliver: ESI");
		AZ(vgzgz);

		// VDP_pesi + buf has been pushed from VCL
		push_vdps(ctx, req, vgzgz, gz);
		AN(parent);
		AZ(VDP_Push(ctx, req->vdc, req->ws,
		    &vped_to_parent, parent->nexus.req));

		(void)VDP_DeliverObj(req->vdc, req->objcore);

		/*
		 * only close the pesi VDPs, the others are to run from topreq
		 * delivery
		 */
		vped_close_vdp(req, 0, &VDP_pesi);
		vped_close_vdp(req, 0, &VDP_pesi_buf);
		return;
	}
	VSLdbg(req, "vped_deliver: T_SUBREQ");

	push_vdps(ctx, req, vgzgz, gz);
	AZ(VDP_Push(ctx, req->vdc, req->ws,
	    &vped_to_parent, parent->nexus.req));

	node_mutate_prep(tree, node);

	/* our caller, cnt_transmit(), releases the refs to boc and oc when we
	 * return, so in order to hand this request to the topreq thread, we
	 * need to gain one more
	 */
	node->subreq.req = req;
	node->subreq.boc = HSH_RefBoc(req->objcore);
	HSH_Ref(req->objcore);
	node->subreq.oc = req->objcore;

	node->subreq.shared_lock = &tree->nodes_lock;
	AZ(pthread_cond_init(&node->subreq.cond, NULL));

	AZ(node->subreq.done);

	node_mutate_lock(tree, node, T_SUBREQ, ST_DATA);
	node_mutate_unlock(tree);

	/*
	 * to be done in the other thread:
	 * - VDP_DeliverObj()
	 * - VDP_Close()
	 *
	 * from vped_task():
	 * - req_fini()
	 */
	return;
}

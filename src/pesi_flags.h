#define PF_HAS_TASK		1U
/* vcl-controlled flags */
#define PF_CFG_SERIAL		(1U<<1)
#define PF_CFG_THREAD		(1U<<2)
#define PF_CFG_ONERROR_CONTINUE	(1U<<3)

#define PF_CFG_DEFAULT	PF_CFG_THREAD

#define PF_MASK_CFG			\
	( PF_CFG_SERIAL			\
	| PF_CFG_THREAD			\
	| PF_CFG_ONERROR_CONTINUE	\
	)
